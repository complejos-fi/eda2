# Bienvenido :D

Este es el repositorio con código de EDA 2 de los complejos de la FI :p

## Consulta

Para consultar el código del repositorio debes de guiarte de las ramas, 
 cada una tiene el nombre de un contribuidor, ahí procuramos subir nuestro
 codigo tanto para nosotros tener el control como para compartirlo. No
 cometas actos deshonestos.

## Contribución

Para contribuir debes de crear tu rama 

```shell
git branch <nombre>
```

Y debes de trabajar con esa rama de ahora en adelante

```shell
git push origin <nombre>
```

## Reglas

- No hacer uso deshonesto de este código, esto incluye hacerlo
 pasar por tuyo, venderlo, etc.

- Mantener el orden de directorios en la rama. 
    - Proyectos
    - Prácticas
    - Extra

